-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cadastros
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `clientes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Identificador unico do sistema, este campo nao pode ser em branco\n',
  `nome` varchar(45) NOT NULL COMMENT 'Nome do Cliente, campo nao aceita nulo',
  `rg` varchar(45) DEFAULT NULL COMMENT 'RG do cliente, nao ha um padrao correto, cada estado possui um',
  `telefone` varchar(20) DEFAULT NULL,
  `celular` varchar(20) NOT NULL,
  `limite_credito` decimal(10,2) NOT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `bairro` varchar(45) DEFAULT NULL,
  `cidade` varchar(45) DEFAULT NULL,
  `estado` char(2) DEFAULT NULL,
  `dat_ultima_venda` datetime DEFAULT NULL,
  `cpf` varchar(14) NOT NULL,
  `dat_nascimento` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clientes_cpf_uindex` (`cpf`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (4,'Nome Cliente','54601','19936415522','19981522980',100.00,'Nome da Rua','100','Distrito Industrial','vargem','SP','2018-10-16 00:00:00','76132550771','2018-10-16');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedores`
--

DROP TABLE IF EXISTS `fornecedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `fornecedores` (
  `idFornecedor` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(255) NOT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `telefone` varchar(45) NOT NULL,
  `celular` varchar(45) NOT NULL,
  `nome_contato` varchar(45) NOT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `bairro` varchar(45) DEFAULT NULL,
  `cidade` varchar(45) DEFAULT NULL,
  `estado` char(2) DEFAULT NULL,
  `cnpj` varchar(20) NOT NULL,
  `inscricao` varchar(14) NOT NULL,
  `dat_ultima_compra` datetime DEFAULT NULL,
  PRIMARY KEY (`idFornecedor`),
  UNIQUE KEY `fornecedores_cnpj_uindex` (`cnpj`),
  UNIQUE KEY `fornecedores_razao_social_uindex` (`razao_social`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedores`
--

LOCK TABLES `fornecedores` WRITE;
/*!40000 ALTER TABLE `fornecedores` DISABLE KEYS */;
INSERT INTO `fornecedores` VALUES (2,'Joaquim Benedito\'s','Mesquita do Torresminho','199884411','19911442233','Weslei','Fredirico Maia','2','Centro','São João da Boa Vista','SP','78478087000102','560415656','2018-09-06 00:00:00'),(3,'Julio Cesar','Esse cara é bom','','','1','Batista Figueiredo','','','','1','78478087000101','','2018-09-06 00:00:00');
/*!40000 ALTER TABLE `fornecedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `codigo_barras` varchar(13) DEFAULT NULL,
  `dat_ultima_compra` datetime DEFAULT NULL,
  `dat_ultima_venda` datetime DEFAULT NULL,
  `vlr_custo` decimal(10,2) NOT NULL,
  `vlr_venda` decimal(10,2) NOT NULL,
  `margem_lucro` decimal(10,3) DEFAULT NULL,
  `estoque` int(11) NOT NULL,
  `idFornecedor` int(11) NOT NULL,
  PRIMARY KEY (`id`,`idFornecedor`),
  KEY `fk_produtos_fornecedores` (`id`),
  KEY `fk_fornecedor_idx` (`idFornecedor`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos`
--

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` VALUES (2,'teste','teste','2018-10-12 00:00:00','2018-10-12 00:00:00',0.00,0.00,0.000,0,2),(3,'Teste 2','Teste 2','2018-10-12 00:00:00','2018-10-12 00:00:00',0.00,0.00,0.000,0,3),(4,'Teste 2','Teste 2','2018-10-12 00:00:00','2018-10-12 00:00:00',0.10,0.00,0.000,0,3),(5,'Teste aq','Teste 3','2018-10-12 00:00:00','2018-10-12 00:00:00',0.00,0.00,0.000,0,2),(6,'teste 20','te 20','2018-10-12 00:00:00','2018-10-12 00:00:00',0.00,0.00,0.000,0,2),(7,'teste 40','teste ','2018-10-13 00:00:00','2018-10-13 00:00:00',0.00,0.00,0.000,0,3);
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-16 18:23:06
