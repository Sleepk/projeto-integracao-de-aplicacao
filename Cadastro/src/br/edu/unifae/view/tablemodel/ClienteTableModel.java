package br.edu.unifae.view.tablemodel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import br.com.caelum.stella.format.CPFFormatter;
import br.edu.unifae.projetointegracao.model.Cliente;

/**
 * 
 * 
 * Informa��es adicionais procurar o item:
 * 
 * {@link }https://www.devmedia.com.br/jtable-utilizando-o-componente-em-interfaces-graficas-swing/28857
 * 
 * 
 * exemplo do github: https://github.com/rosicleiafrasson/exemploJTable
 */
public class ClienteTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private static SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat dateTimeformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private List<Cliente> clientes = new ArrayList<>();

	/**
	 * Funcao load para carregar os itens
	 * 
	 * fireTableDataChange, notifica a tabela que possa carregar os itens
	 * 
	 * @param cliente
	 */
	public void load(List<Cliente> cliente) {
		this.clientes = cliente;
		fireTableDataChanged();
	}

	/**
	 * Coletara o indice de clientes.
	 * 
	 * @param index
	 * @return
	 */
	public Cliente getClienteAt(int index) {
		if (this.clientes.size() <= 0) {
			return null;
		}
		return this.clientes.get(index);
	}

	/*
	 * O m�todo getColumnCount deve retornar a quantidade total de colunas que a
	 * JTable deve usar para montar a tabela. No nosso caso, o cabe�alho das colunas
	 * encontra-se armazenado no vetor colunas, desta forma, o m�todo retorna o
	 * tamanho do vetor colunas.
	 */
	@Override
	public int getColumnCount() {
		return 13;
	}

	/**
	 * O m�todo getRowCount retorna a quantidade total de colunas que a JTable deve
	 * usar para montar a tabela. Como os dados est�o armazenados na List linhas, o
	 * m�todo retorna o tamanho da mesma.
	 */
	@Override
	public int getRowCount() {
		return clientes.size();
	}

	/**
	 * O m�todo getValueAt retorna o conte�do da c�lula especificada no par�metro.
	 * 
	 * 78478087000102
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		CPFFormatter formatador = new CPFFormatter();

		if (this.clientes.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Nao ha Clientes para exibir");
			return null;
		}
		Cliente cliente = clientes.get(rowIndex);

		switch (columnIndex) {
		case 0:
			return cliente.getNome();
		case 1:
			return cliente.getRg();
		case 2:

			return formatador.format(cliente.getCpf());

		case 3:

			if (cliente.getDat_nascimento() == null) {
				return cliente.getDat_nascimento();
			} else {
				return dateformat.format(cliente.getDat_nascimento());
			}

		case 4:
			return cliente.getCelular();
		case 5:
			return cliente.getTelefone();
		case 6:
			return cliente.getEndereco();
		case 7:
			return cliente.getNumero();
		case 8:
			return cliente.getBairro();
		case 9:
			return cliente.getCidade();
		case 10:
			return cliente.getEstado();
		case 11:
			return cliente.getLimite_credito();

		case 12:
			if (cliente.getDat_ultima_venda() == null) {
				return cliente.getDat_ultima_venda();
			} else {
				return dateTimeformat.format(cliente.getDat_ultima_venda());
			}

		}
		return null;
	}

	/*
	 * O m�todo getColumnName retorna o nome da coluna referente ao �ndice
	 * especificado por par�metro. Como os nomes das colunas est�o armazenados no
	 * vetor colunas, o m�todo retorna a String armazenada na posi��o especificada
	 * no par�metro.
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int columnIndex) {

		switch (columnIndex) {
		case 0:
			return "Nome";
		case 1:
			return "RG";
		case 2:
			return "CPF";
		case 3:
			return "Data Nascimento";
		case 4:
			return "Celular";
		case 5:
			return "Telefone";
		case 6:
			return "Endereco";
		case 7:
			return "Numero";
		case 8:
			return "Bairro";
		case 9:
			return "Cidade";
		case 10:
			return "Estado";
		case 11:
			return "Limite credito";
		case 12:
			return "Data ultima venda";
		case 13:

			return "Limite credito";

		}
		return null;
	}
}
