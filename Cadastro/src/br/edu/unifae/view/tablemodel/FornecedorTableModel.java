package br.edu.unifae.view.tablemodel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import br.com.caelum.stella.format.CNPJFormatter;
import br.edu.unifae.projetointegracao.model.Fornecedor;

/**
 * 
 * 
 * Informa��es adicionais procurar o item:
 * 
 * {@link }https://www.devmedia.com.br/jtable-utilizando-o-componente-em-interfaces-graficas-swing/28857
 * 
 * 
 * exemplo do github: https://github.com/rosicleiafrasson/exemploJTable
 */
public class FornecedorTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private static SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat dateTimeformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private List<Fornecedor> fornecedores = new ArrayList<>();

	/**
	 * Funcao load para carregar os itens
	 * 
	 * fireTableDataChange, notifica a tabela que possa carregar os itens
	 * 
	 * @param cliente
	 */
	public void load(List<Fornecedor> cliente) {
		this.fornecedores = cliente;
		fireTableDataChanged();
	}

	/**
	 * Coletara o indice de fornecedores.
	 * 
	 * @param index
	 * @return
	 */
	public Fornecedor getFornecedorAt(int index) {
		if (this.fornecedores.size() <= 0) {
			return null;
		}
		return this.fornecedores.get(index);
	}

	/*
	 * O m�todo getColumnCount deve retornar a quantidade total de colunas que a
	 * JTable deve usar para montar a tabela. No nosso caso, o cabe�alho das colunas
	 * encontra-se armazenado no vetor colunas, desta forma, o m�todo retorna o
	 * tamanho do vetor colunas.
	 */
	@Override
	public int getColumnCount() {
		return 13;
	}

	/**
	 * O m�todo getRowCount retorna a quantidade total de colunas que a JTable deve
	 * usar para montar a tabela. Como os dados est�o armazenados na List linhas, o
	 * m�todo retorna o tamanho da mesma.
	 */
	@Override
	public int getRowCount() {
		return fornecedores.size();
	}

	/**
	 * O m�todo getValueAt retorna o conte�do da c�lula especificada no par�metro.
	 * 
	 *
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		CNPJFormatter formatador = new CNPJFormatter();

		if (this.fornecedores.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Nao ha Fornecedors para exibir");
			return null;
		}

		Fornecedor fornecedor = fornecedores.get(rowIndex);

		switch (columnIndex) {
		case 0:
			return fornecedor.getRazao_social();
		case 1:
			return fornecedor.getNome_fantasia();
		case 2:
			return fornecedor.getTelefone();
		case 3:
			return fornecedor.getCelular();
		case 4:
			return fornecedor.getNome_contato();
		case 5:
			return fornecedor.getEndereco();
		case 6:
			return fornecedor.getNumero();
		case 7:
			return fornecedor.getBairro();
		case 8:
			return fornecedor.getCidade();
		case 9:
			return fornecedor.getEstado();
		case 10:
			return formatador.format(fornecedor.getCnpj());
		case 11:
			return fornecedor.getInscricao();
		case 12:
			if (fornecedor.getDat_ultima_compra() == null) {
				return fornecedor.getDat_ultima_compra();
			} else {
				return dateTimeformat.format(fornecedor.getDat_ultima_compra());
			}
		}
		return null;

	}

	/*
	 * O m�todo getColumnName retorna o nome da coluna referente ao �ndice
	 * especificado por par�metro. Como os nomes das colunas est�o armazenados no
	 * vetor colunas, o m�todo retorna a String armazenada na posi��o especificada
	 * no par�metro.
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int columnIndex) {

		switch (columnIndex) {
		case 0:
			return "Raz�o Social";
		case 1:
			return "Nome Fantasia";
		case 2:
			return "Telefone";
		case 3:
			return "Celular";
		case 4:
			return "Nome Contato";
		case 5:
			return "Endereco";
		case 6:
			return "Numero";
		case 7:
			return "Bairro";
		case 8:
			return "Cidade";
		case 9:
			return "Estado";
		case 10:
			return "CNPJ";
		case 11:
			return "Inscri��o";
		case 12:
			return "Data Ultima Venda";
		}
		return null;
	}
}
