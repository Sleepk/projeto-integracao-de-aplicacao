package br.edu.unifae.view.tablemodel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import br.edu.unifae.projetointegracao.model.Produto;

public class ProdutoTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3409960216745827282L;
	private static SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
	private List<Produto> produtos = new ArrayList<>();

	public void load(List<Produto> produtos) {
		this.produtos = produtos;
		fireTableDataChanged();
	}

	public Produto getProdutoAt(int index) {
		if (this.produtos.size() <= 0) {
			return null;
		}
		return this.produtos.get(index);
	}

	@Override
	public int getColumnCount() {
		return 9;
	}

	@Override
	public int getRowCount() {
		return produtos.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		Produto produto = produtos.get(rowIndex);
		

		if (this.produtos.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Nao ha Produtos para exibir");
			return null;
		}
		

		switch (columnIndex) {
		case 0:
			return produto.getNome();
		case 1:
			return produto.getCodigoBarra();
		case 2:
			return produto.getEstoque();
		case 3:
			return produto.getFornecedor().getNome_fantasia();
		case 4:
			return produto.getMargemLucro();
		case 5:
			if (produto.getUltimaCompra() == null) {
				return produto.getUltimaCompra();
			} else {
				return dateformat.format(produto.getUltimaCompra());
			}
		case 6:
			if (produto.getUltimaVenda() == null) {
				return produto.getUltimaVenda();
			} else {
				return dateformat.format(produto.getUltimaVenda());
			}
		case 7:
			return produto.getValorCusto();
		case 8:
			return produto.getValorVenda();
		}
		return null;

	}

	@Override
	public String getColumnName(int columnIndex) {

		switch (columnIndex) {
		case 0:
			return "Nome";
		case 1:
			return "Codigo Barra";
		case 2:
			return "Estoque";
		case 3:
			return "Nome Fornecedor";
		case 4:
			return "Margem Lucro";
		case 5:
			return "Ultima Compra";
		case 6:
			return "Ultima Venda";
		case 7:
			return "Valor Custo";
		case 8:
			return "Valor Venda";
		}
		return null;
	}

}
