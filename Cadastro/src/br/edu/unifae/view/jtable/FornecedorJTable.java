package br.edu.unifae.view.jtable;

import java.util.List;

import javax.swing.JTable;

import br.edu.unifae.projetointegracao.model.Fornecedor;
import br.edu.unifae.view.tablemodel.FornecedorTableModel;


public class FornecedorJTable extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4875433198326211355L;
	
	/**
	 * Metodo para instanciar a Jtable
	 */
	private FornecedorTableModel tableModel;

	public FornecedorJTable() {
		this.tableModel = new FornecedorTableModel();
		setModel(this.tableModel);
	}

	/**
	 * Metodo para carregar a lista de usuarios
	 * 
	 * @param fornecedor
	 */
	public void load(List<Fornecedor> fornecedor) {
		this.tableModel.load(fornecedor);
	}

	/**
	 * Metodo para selecionar a linha e retornar o indice clicado
	 * 
	 * @return index
	 */
	public Fornecedor getFornecedorSelecionado() {
		int index = getSelectedRow();
		return this.tableModel.getFornecedorAt(index);
	}

}
