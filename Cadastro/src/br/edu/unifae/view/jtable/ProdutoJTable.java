package br.edu.unifae.view.jtable;

import java.util.List;

import javax.swing.JTable;

import br.edu.unifae.projetointegracao.model.Produto;
import br.edu.unifae.view.tablemodel.ProdutoTableModel;

public class ProdutoJTable extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9084439237232761815L;
	private ProdutoTableModel tableModel;

	public ProdutoJTable() {
		this.tableModel = new ProdutoTableModel();
		setModel(this.tableModel);
	}

	/**
	 * Metodo para carregar a lista de usuarios
	 * 
	 * @param produto
	 */
	public void load(List<Produto> produto) {
		this.tableModel.load(produto);
	}

	/**
	 * Metodo para selecionar a linha e retornar o indice clicado
	 * 
	 * @return index
	 */
	public Produto getProdutoSelecionado() {
		int index = getSelectedRow();
		Produto produto = this.tableModel.getProdutoAt(index);
		return produto;
	}
}
