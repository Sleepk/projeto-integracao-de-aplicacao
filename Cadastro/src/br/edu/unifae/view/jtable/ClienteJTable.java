package br.edu.unifae.view.jtable;

import java.util.List;

import javax.swing.JTable;

import br.edu.unifae.projetointegracao.model.Cliente;
import br.edu.unifae.view.tablemodel.ClienteTableModel;

public class ClienteJTable extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4875433198326211355L;

	/**
	 * Metodo para instanciar a Jtable
	 */
	private ClienteTableModel tableModel;

	public ClienteJTable() {
		this.tableModel = new ClienteTableModel();
		setModel(this.tableModel);
	}

	/**
	 * Metodo para carregar a lista de usuarios
	 * 
	 * @param cliente
	 */
	public void load(List<Cliente> cliente) {
		this.tableModel.load(cliente);
	}

	/**
	 * Metodo para selecionar a linha e retornar o indice clicado
	 * 
	 * @return index
	 */
	public Cliente getClienteSelecionado() {
		int index = getSelectedRow();
		Cliente cliente = this.tableModel.getClienteAt(index);
		return cliente;
	}

}
