package br.edu.unifae.projetointegracao.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

import br.edu.unifae.projetointegracao.dao.ProdutoDAO;
import br.edu.unifae.projetointegracao.exception.NegocioException;
import br.edu.unifae.projetointegracao.model.Produto;

public class ProdutoService {

	public String escreveArquivoProduto = "cadastroProduto.txt";

	public void calculaCusto(Produto produto) {
		Double teste = ((produto.getMargemLucro() / 100) * produto.getValorCusto()) + produto.getValorCusto();
		produto.setValorVenda(teste);
	}

	public void cadastrar(Produto produto) throws NegocioException {
		try {
			ProdutoDAO pessoaDAO = new ProdutoDAO();
			int valor = pessoaDAO.Save(produto);
			escritor(escreveArquivoProduto, produto, valor);
		} catch (Exception exception) {
			throw new NegocioException(exception.getMessage());
		}
	}

	public String produtoIntegracao(Produto produto, int valor) {
		
		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		
		StringBuilder builder = new StringBuilder();
		switch (valor) {
		case 0:
			builder.append("0");
			break;
		case 1:
			builder.append("1");
			break;
		case 2:
			builder.append("2");
			builder.append("|");
			builder.append(produto.getId());
			builder.append(";");
			return builder.toString();
		}
		builder.append("|");
		builder.append(produto.getNome());
		builder.append("|");
		builder.append(produto.getCodigoBarra());
		builder.append("|");
		builder.append(produto.getValorCusto());
		builder.append("|");
		builder.append(produto.getValorVenda());
		builder.append("|");
		builder.append(produto.getMargemLucro());
		builder.append("|");
		builder.append(produto.getEstoque());
		builder.append("|");
		builder.append(dateformat.format(produto.getUltimaVenda()));
		builder.append("|");
		builder.append(dateformat.format(produto.getUltimaCompra()));
		builder.append("|");
		builder.append(produto.getId());
		builder.append("|");
		builder.append(produto.getFornecedor().getId());
		builder.append("|");
		builder.append(produto.getFornecedor().getNome_fantasia());
		builder.append(";");
		return builder.toString();
	}

	public void escritor(String path, Produto produto, int valor) throws IOException {
		BufferedWriter buffWrite = new BufferedWriter(new FileWriter(path, true));
		buffWrite.append(produtoIntegracao(produto, valor) + "\n");
		buffWrite.close();
	}

	public void removePessoa(int idPessoa) throws NegocioException {
		try {
			ProdutoDAO produtoDAO = new ProdutoDAO();
			Produto produtoDeletado = produtoDAO.findById(idPessoa);
			int deletar = produtoDAO.deletar(idPessoa);
			escritor(escreveArquivoProduto, produtoDeletado, deletar);
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new NegocioException(exception.getMessage());
		}
	}

}
