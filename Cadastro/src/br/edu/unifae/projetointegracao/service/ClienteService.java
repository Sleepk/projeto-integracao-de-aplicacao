package br.edu.unifae.projetointegracao.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import br.edu.unifae.projetointegracao.dao.ClienteDAO;
import br.edu.unifae.projetointegracao.exception.NegocioException;
import br.edu.unifae.projetointegracao.exception.PersistenciaExcpetion;
import br.edu.unifae.projetointegracao.model.Cliente;

public class ClienteService {

	public String escreveArquivoCliente = "cadastrocliente.txt";

	ClienteDAO clienteDAO = null;

	public void cadastrar(Cliente cliente) throws NegocioException {
		try {
			clienteDAO = new ClienteDAO();
			int valor = clienteDAO.Save(cliente);
			escritor(escreveArquivoCliente, cliente, valor);
		} catch (Exception exception) {
			throw new NegocioException(exception.getMessage());
		}
	}

	public String clienteIntegracao(Cliente cliente, int valor) {
		
		

		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dateTimeformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		StringBuilder builder = new StringBuilder();
		switch (valor) {
		case 0:
			builder.append("0");
			break;
		case 1:
			builder.append("1");
			break;

		case 2:
			builder.append("2");
			builder.append("|");
			builder.append(cliente.getId());
			builder.append(";");
			return builder.toString();
		}
		builder.append("|");
		builder.append(cliente.getBairro());
		builder.append("|");
		builder.append(cliente.getCelular());
		builder.append("|");
		builder.append(cliente.getCidade());
		builder.append("|");
		builder.append(cliente.getCpf());
		builder.append("|");
		builder.append(dateformat.format(cliente.getDat_nascimento()));
		builder.append("|");
		builder.append(dateTimeformat.format(cliente.getDat_ultima_venda()));
		builder.append("|");
		builder.append(cliente.getEndereco());
		builder.append("|");
		builder.append(cliente.getEstado());
		builder.append("|");
		builder.append(cliente.getId());
		builder.append("|");
		builder.append(cliente.getLimite_credito());
		builder.append("|");
		builder.append(cliente.getNome().toUpperCase());
		builder.append("|");
		builder.append(cliente.getNumero());
		builder.append("|");
		builder.append(cliente.getRg());
		builder.append("|");
		builder.append(cliente.getTelefone());
		builder.append(";");
		return builder.toString();
	}

	public void escritor(String path, Cliente cliente, int valor) throws IOException {
		BufferedWriter buffWrite = new BufferedWriter(new FileWriter(path, true));
		buffWrite.append(clienteIntegracao(cliente, valor) + "\n");
		buffWrite.close();
	}

	public List<Cliente> buscaPorNome(String nome)throws NegocioException {
		clienteDAO = new ClienteDAO();
		try {
			return clienteDAO.findByNome(nome);
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new NegocioException(exception.getMessage());
		}
	}

	public void removePessoa(int idPessoa) throws NegocioException {
		try {
			clienteDAO = new ClienteDAO();
			Cliente clienteDeletado = clienteDAO.findById(idPessoa);
			int deletar = clienteDAO.deletar(idPessoa);
			escritor(escreveArquivoCliente, clienteDeletado, deletar);
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new NegocioException(exception.getMessage());
		}
	}
}