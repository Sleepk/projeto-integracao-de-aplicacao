package br.edu.unifae.projetointegracao.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import br.edu.unifae.projetointegracao.dao.FornecedorDAO;
import br.edu.unifae.projetointegracao.exception.NegocioException;
import br.edu.unifae.projetointegracao.model.Fornecedor;

public class FornecedorService {
	public String escreveArquivoFornecedor = "cadastroFornecedor.txt";

	public Fornecedor buscaPorId(int id) throws NegocioException {
		Fornecedor fornecedor = null;
		try {
			FornecedorDAO fornecedorDAO = new FornecedorDAO();
			fornecedor = fornecedorDAO.findById(id);
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
		return fornecedor;
	}

	public List<Fornecedor> buscarPorNomeFantasia() throws NegocioException {
		try {
			FornecedorDAO fornecedorDAO = new FornecedorDAO();
			return fornecedorDAO.listarNomeFantasia();
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
	}

	public List<Fornecedor> buscarTodos() throws NegocioException {
		try {
			FornecedorDAO fornecedorDAO = new FornecedorDAO();
			return fornecedorDAO.listarTodos();
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
	}

	public void cadastrar(Fornecedor fornecedor) throws NegocioException {
		try {
			FornecedorDAO fornecedorDAO = new FornecedorDAO();
			int valor = fornecedorDAO.Save(fornecedor);
			escritor(escreveArquivoFornecedor, fornecedor, valor);
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new NegocioException(exception.getMessage());
		}
	}

	public void escritor(String path, Fornecedor fornecedor, int valor) throws IOException {
		BufferedWriter buffWrite = new BufferedWriter(new FileWriter(path, true));
		buffWrite.append(fornecedorIntegracao(fornecedor, valor) + "\n");
		buffWrite.close();
	}

	public String fornecedorIntegracao(Fornecedor fornecedor, int valor) {
		
		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		
		StringBuilder builder = new StringBuilder();

		switch (valor) {
		case 0:
			builder.append("0");
			break;
		case 1:
			builder.append("1");
			break;

		case 3:
			builder.append("2");
			builder.append("|");
			builder.append(fornecedor.getId());
			builder.append(";");
			return builder.toString();
		}
		builder.append("|");
		builder.append(fornecedor.getRazao_social());
		builder.append("|");
		builder.append(fornecedor.getNome_fantasia());
		builder.append("|");
		builder.append(fornecedor.getTelefone());
		builder.append("|");
		builder.append(fornecedor.getCelular());
		builder.append("|");
		builder.append(fornecedor.getNome_contato());
		builder.append("|");
		builder.append(fornecedor.getEndereco());
		builder.append("|");
		builder.append(fornecedor.getNumero());
		builder.append("|");
		builder.append(fornecedor.getBairro());
		builder.append("|");
		builder.append(fornecedor.getCidade());
		builder.append("|");
		builder.append(fornecedor.getEstado());
		builder.append("|");
		builder.append(fornecedor.getCnpj());
		builder.append("|");
		builder.append(fornecedor.getInscricao());
		builder.append("|");
		builder.append(dateformat.format(fornecedor.getDat_ultima_compra()));
		builder.append("|");
		builder.append(fornecedor.getId());
		builder.append(";");
		return builder.toString();
	}

	public void removeFornecedor(int idFornecedor) throws NegocioException {
		try {
			FornecedorDAO fornecedorDAO = new FornecedorDAO();
			Fornecedor fornecedorDeletado = fornecedorDAO.findById(idFornecedor);
			int valor = fornecedorDAO.deletar(idFornecedor);
			escritor(escreveArquivoFornecedor, fornecedorDeletado, valor);
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new NegocioException(exception.getMessage());
		}
	}

}
