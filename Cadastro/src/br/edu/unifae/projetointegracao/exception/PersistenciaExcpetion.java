package br.edu.unifae.projetointegracao.exception;


/**
 * Classe responsavel por jogar exception ao tentar fazer uma persistencia.
 * @author julio
 *
 */
public class PersistenciaExcpetion extends Exception {

	private static final long serialVersionUID = -8796457926599751430L;

	public PersistenciaExcpetion(String msg, Exception exception) {
		super(msg, exception);
	}
	
	public PersistenciaExcpetion(String msg) {
		super(msg);
	}
	
}
