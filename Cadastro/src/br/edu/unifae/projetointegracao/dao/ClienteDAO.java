package br.edu.unifae.projetointegracao.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.edu.unifae.projetointegracao.conexaoUtil.ConexaoUtil;
import br.edu.unifae.projetointegracao.exception.PersistenciaExcpetion;
import br.edu.unifae.projetointegracao.model.Cliente;
import br.edu.unifae.projetointegracao.model.generico.GenericoDAO;

public class ClienteDAO implements GenericoDAO<Cliente> {

	public Cliente createCliente(ResultSet result) throws SQLException {

		Cliente cliente = new Cliente();

		cliente.setBairro(result.getString("bairro"));
		cliente.setCelular(result.getString("celular"));
		cliente.setCidade(result.getString("cidade"));
		cliente.setCpf(result.getString("cpf"));
		cliente.setDat_nascimento(result.getDate("dat_nascimento"));
		cliente.setDat_ultima_venda((java.util.Date) result.getObject("dat_ultima_venda"));
		cliente.setEndereco(result.getString("endereco"));
		cliente.setEstado(result.getString("estado"));
		cliente.setId(result.getInt("Id"));
		cliente.setLimite_credito(result.getBigDecimal("limite_credito"));
		cliente.setNome(result.getString("nome"));
		cliente.setNumero(result.getString("numero"));
		cliente.setRg(result.getString("rg"));
		cliente.setTelefone(result.getString("telefone"));

		return cliente;
	}

	@Override
	public int Save(Cliente cliente) throws PersistenciaExcpetion {
		Connection connection = null;
		PreparedStatement preparaStatement = null;
		try {
			String sql;
			int valor;
			connection = ConexaoUtil.getInstance().getConnection();

			if (cliente.getId() == null) {

				sql = "insert into clientes (bairro,celular,cidade,cpf,dat_nascimento, dat_ultima_venda,endereco,estado,limite_credito,nome, numero, rg, telefone) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
				valor = 0;
			} else {
				sql = "UPDATE clientes set bairro=?, celular=?,cidade=?,cpf=?,dat_nascimento=?, dat_ultima_venda=?,endereco=?,estado=?,limite_credito=?,nome=?, numero=?, rg=?, telefone=?"
						+ " WHERE id = ?";

				valor = 1;
			}
			preparaStatement = preparaStatement(cliente, connection, sql, Statement.RETURN_GENERATED_KEYS);

			preparaStatement.execute();
			preparaStatement.close();
			connection.close();
			return valor;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
	}

	@Override
	public List<Cliente> listarTodos() throws PersistenciaExcpetion {
		List<Cliente> listaPessoas = new ArrayList<Cliente>();
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "SELECT * FROM clientes";

			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Cliente cliente = createCliente(resultSet);
				listaPessoas.add(cliente);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return listaPessoas;
	}

	@Override
	public int deletar(int id) throws PersistenciaExcpetion {
		int valor;
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "DELETE FROM clientes WHERE id = ?";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			statement.execute();
			valor = 2;
			statement.close();
			connection.close();
			return valor;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}

	}

	@Override
	public Cliente findById(int id) throws PersistenciaExcpetion {
		Connection connection = null;
		Cliente cliente = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConexaoUtil.getInstance().getConnection();
			preparedStatement = connection.prepareStatement("select * from clientes where id = ?");

			preparedStatement.setLong(1, id);

			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				cliente = createCliente(resultSet);
				resultSet.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return cliente;
	}

	private PreparedStatement preparaStatement(Cliente cliente, Connection connection, String sql,
			int returnGeneratedKeys) throws SQLException {
		PreparedStatement stmt = connection.prepareStatement(sql);
		stmt.setString(1, cliente.getBairro());
		stmt.setString(2, cliente.getCelular());
		stmt.setString(3, cliente.getCidade());
		stmt.setString(4, cliente.getCpf());
		stmt.setDate(5, new Date(cliente.getDat_nascimento().getTime()));
		stmt.setObject(6, new java.sql.Timestamp(cliente.getDat_ultima_venda().getTime()));
		stmt.setString(7, cliente.getEndereco());
		stmt.setString(8, cliente.getEstado());
		stmt.setBigDecimal(9, cliente.getLimite_credito());
		stmt.setString(10, cliente.getNome());
		stmt.setString(11, cliente.getNumero());
		stmt.setString(12, cliente.getRg());
		stmt.setString(13, cliente.getTelefone());
		if (cliente.getId() != null) {
			stmt.setLong(14, cliente.getId());
		} else {
			Integer id = returnGeneratedKeys;
			cliente.setId(id);
		}

		return stmt;
	}

	public List<Cliente> findByNome(String nome) throws PersistenciaExcpetion {
		List<Cliente> listaPessoas = new ArrayList<Cliente>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConexaoUtil.getInstance().getConnection();

			preparedStatement = connection.prepareStatement("SELECT * FROM clientes where nome like ? ");

			preparedStatement.setString(1, "%" + nome + "%");

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Cliente cliente = createCliente(resultSet);
				listaPessoas.add(cliente);
			}
			preparedStatement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return listaPessoas;
	}
}
