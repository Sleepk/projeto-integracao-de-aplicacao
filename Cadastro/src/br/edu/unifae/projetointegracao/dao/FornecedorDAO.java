package br.edu.unifae.projetointegracao.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.edu.unifae.projetointegracao.conexaoUtil.ConexaoUtil;
import br.edu.unifae.projetointegracao.exception.PersistenciaExcpetion;
import br.edu.unifae.projetointegracao.model.Fornecedor;
import br.edu.unifae.projetointegracao.model.generico.GenericoDAO;

public class FornecedorDAO implements GenericoDAO<Fornecedor> {

	public Fornecedor createFornecedor(ResultSet result) throws SQLException {

		Fornecedor fornecedor = new Fornecedor();

		fornecedor.setId(result.getInt("idFornecedor"));
		fornecedor.setRazao_social(result.getString("razao_social"));
		fornecedor.setNome_fantasia(result.getString("nome_fantasia"));
		fornecedor.setTelefone(result.getString("telefone"));
		fornecedor.setCelular(result.getString("celular"));
		fornecedor.setNome_contato(result.getString("nome_contato"));
		fornecedor.setEndereco(result.getString("endereco"));
		fornecedor.setNumero(result.getString("numero"));
		fornecedor.setBairro(result.getString("bairro"));
		fornecedor.setCidade(result.getString("cidade"));
		fornecedor.setEstado(result.getString("estado"));
		fornecedor.setCnpj(result.getString("cnpj"));
		fornecedor.setInscricao(result.getString("inscricao"));
		fornecedor.setDat_ultima_compra((java.util.Date)result.getObject("dat_ultima_compra"));

		return fornecedor;
	}

	@Override
	public int Save(Fornecedor fornecedor) throws PersistenciaExcpetion {
		Connection connection = null;
		PreparedStatement stmt = null;
		try {
			String sql;
			int valor;
			connection = ConexaoUtil.getInstance().getConnection();

			if (fornecedor.getId() == null) {

				sql = "insert into fornecedores (razao_social,nome_fantasia,telefone,celular,nome_contato,endereco,numero,bairro,cidade,estado,cnpj,inscricao,dat_ultima_compra) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
				valor = 0;
			} else {
				sql = "update fornecedores set  razao_social=?,nome_fantasia=?,telefone=?,celular=?,nome_contato=?,endereco=?,numero=?,bairro=?,cidade=?,estado=?,cnpj=?,inscricao=?,dat_ultima_compra=?"
						+ "where idFornecedor = ?";
				valor = 1;
			}

			stmt = preparaStatement(fornecedor, connection, sql, Statement.RETURN_GENERATED_KEYS);

			stmt.execute();
			stmt.close();
			connection.close();
			return valor;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}

	}

	private PreparedStatement preparaStatement(Fornecedor fornecedor, Connection connection, String sql,
			int returnGeneratedKeys) throws SQLException {

		PreparedStatement stmt = connection.prepareStatement(sql);

		stmt.setString(1, fornecedor.getRazao_social());
		stmt.setString(2, fornecedor.getNome_fantasia());
		stmt.setString(3, fornecedor.getTelefone());
		stmt.setString(4, fornecedor.getCelular());
		stmt.setString(5, fornecedor.getNome_contato());
		stmt.setString(6, fornecedor.getEndereco());
		stmt.setString(7, fornecedor.getNumero());
		stmt.setString(8, fornecedor.getBairro());
		stmt.setString(9, fornecedor.getCidade());
		stmt.setString(10, fornecedor.getEstado());
		stmt.setString(11, fornecedor.getCnpj());
		stmt.setString(12, fornecedor.getInscricao());
		stmt.setObject(13, new java.sql.Timestamp(fornecedor.getDat_ultima_compra().getTime()));
		if (fornecedor.getId() != null) {
			stmt.setLong(14, fornecedor.getId());
		} else {
			Integer id = returnGeneratedKeys;
			fornecedor.setId(id);
		}

		return stmt;

	}

	@Override
	public List<Fornecedor> listarTodos() throws PersistenciaExcpetion {
		List<Fornecedor> listaPessoas = new ArrayList<Fornecedor>();
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "SELECT * FROM fornecedores";

			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Fornecedor fornecedor = createFornecedor(resultSet);
				listaPessoas.add(fornecedor);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return listaPessoas;
	}

	@Override
	public int deletar(int id) throws PersistenciaExcpetion {
		int valor;

		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "DELETE FROM fornecedores WHERE idFornecedor = ?";
			valor = 3;
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);

			statement.execute();
			connection.close();
			return valor;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}

	}

	@Override
	public Fornecedor findById(int id) throws PersistenciaExcpetion {
		Connection connection = null;
		Fornecedor fornecedor = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConexaoUtil.getInstance().getConnection();
			preparedStatement = connection.prepareStatement("select * from fornecedores where idFornecedor = ?");

			preparedStatement.setLong(1, id);

			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				fornecedor = createFornecedor(resultSet);
				resultSet.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return fornecedor;
	}

	public List<Fornecedor> listarNomeFantasia() throws PersistenciaExcpetion {
		List<Fornecedor> listaPessoas = new ArrayList<Fornecedor>();
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "SELECT idFornecedor, nome_fantasia FROM fornecedores";

			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setNome_fantasia(resultSet.getString("nome_fantasia"));
				fornecedor.setId(resultSet.getInt("idFornecedor"));

				listaPessoas.add(fornecedor);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return listaPessoas;
	}

}
