package br.edu.unifae.projetointegracao.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.edu.unifae.projetointegracao.conexaoUtil.ConexaoUtil;
import br.edu.unifae.projetointegracao.exception.PersistenciaExcpetion;
import br.edu.unifae.projetointegracao.model.Fornecedor;
import br.edu.unifae.projetointegracao.model.Produto;
import br.edu.unifae.projetointegracao.model.generico.GenericoDAO;

public class ProdutoDAO implements GenericoDAO<Produto> {

	public Produto createProduto(ResultSet result) throws SQLException,
			PersistenciaExcpetion {

		Produto produto = new Produto();

		Fornecedor fornecedor = new Fornecedor();

		produto.setId(result.getInt("id"));
		produto.setNome(result.getString("nome"));
		produto.setCodigoBarra(result.getString("codigo_barras"));
		produto.setValorCusto(result.getDouble("vlr_custo"));
		produto.setValorVenda(result.getDouble("vlr_venda"));
		produto.setMargemLucro(result.getDouble("margem_lucro"));
		produto.setEstoque(result.getInt("estoque"));
		fornecedor.setId(result.getInt("idFornecedor"));
		fornecedor.setNome_fantasia(result.getString("nome_fantasia"));
		produto.setFornecedor(fornecedor);
		produto.setUltimaVenda(result.getDate("dat_ultima_venda"));
		produto.setUltimaCompra(result.getDate("dat_ultima_compra"));

		return produto;
	}

	@Override
	public int Save(Produto produto) throws PersistenciaExcpetion {
		Connection connection = null;
		try {
			String sql;
			int valor;
			connection = ConexaoUtil.getInstance().getConnection();
			if (produto.getId() == null) {
				sql = "insert into produtos (nome, codigo_barras, vlr_custo, vlr_venda, margem_lucro, estoque, idFornecedor, dat_ultima_venda, dat_ultima_compra) VALUES(?,?,?,?,?,?,?,?,?)";
				valor = 0;
			} else {
				sql = "UPDATE produtos set nome=?,  codigo_barras=?,  vlr_custo=?,  vlr_venda=?,  margem_lucro=?,  estoque=?,  idFornecedor=?,dat_ultima_venda=?,  dat_ultima_compra=?"
						+ "where id = ?";
				valor = 1;
			}

			PreparedStatement preparaStatement = preparaStatement(produto,
					connection, sql, Statement.RETURN_GENERATED_KEYS);

			preparaStatement.execute();
			preparaStatement.close();
			connection.close();
			return valor;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
	}

	private PreparedStatement preparaStatement(Produto produto,
			Connection connection, String sql, int returnGeneratedKeys)
			throws SQLException {
		PreparedStatement stmt = connection.prepareStatement(sql);

		stmt.setString(1, produto.getNome());
		stmt.setString(2, produto.getCodigoBarra());
		stmt.setDouble(3, produto.getValorCusto());
		stmt.setDouble(4, produto.getValorVenda());
		stmt.setDouble(5, produto.getMargemLucro());
		stmt.setInt(6, produto.getEstoque());
		stmt.setInt(7, produto.getFornecedor().getId());
		stmt.setDate(8, new Date(produto.getUltimaVenda().getTime()));
		stmt.setDate(9, new Date(produto.getUltimaCompra().getTime()));
		if (produto.getId() != null) {
			stmt.setLong(10, produto.getId());
		} else {
			Integer id = returnGeneratedKeys;
			produto.setId(id);
		}
		return stmt;
	}

	@Override
	public List<Produto> listarTodos() throws PersistenciaExcpetion {
		List<Produto> listaPessoas = new ArrayList<Produto>();
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "SELECT forne.nome_fantasia, prod.* FROM produtos as prod "
					+ "inner join fornecedores as forne "
					+ "on forne.idFornecedor = prod.idFornecedor";

			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Produto produto = createProduto(resultSet);
				listaPessoas.add(produto);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return listaPessoas;
	}

	@Override
	public int deletar(int id) throws PersistenciaExcpetion {

		try {
			int valor;
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "DELETE FROM produtos WHERE id = ?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			statement.execute();
			valor = 2;
			statement.close();
			connection.close();
			return valor;
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}

	}

	public Produto findByNome(String nome) throws PersistenciaExcpetion {

		Connection connection = null;
		Produto produto = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConexaoUtil.getInstance().getConnection();
			preparedStatement = connection
					.prepareStatement("select * from produtos where nome_fantasia = ?");

			preparedStatement.setString(1, nome);

			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				produto = createProduto(resultSet);
				resultSet.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return produto;

	}

	@Override
	public Produto findById(int id) throws PersistenciaExcpetion {
		Connection connection = null;
		Produto produto = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConexaoUtil.getInstance().getConnection();
			preparedStatement = connection
					.prepareStatement("select * from produtos prod "
					+ "inner join fornecedores as forne "
							+ "on forne.idFornecedor = prod.idFornecedor"
							+ " where id = ?");

			preparedStatement.setLong(1, id);

			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				produto = createProduto(resultSet);
				resultSet.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return produto;
	}

}
