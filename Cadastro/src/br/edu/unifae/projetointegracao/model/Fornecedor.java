package br.edu.unifae.projetointegracao.model;

import java.util.Date;

public class Fornecedor {

	private Integer id;

	private String razao_social;

	private String nome_fantasia;

	private String telefone;

	private String celular;

	private String nome_contato;

	private String endereco;

	private String numero;

	private String bairro;

	private String cidade;

	private String estado;

	private String cnpj;

	private String inscricao;

	private Date dat_ultima_compra;

	public Fornecedor() {
		super();
	}

	public Fornecedor(String razao_social, String telefone, String celular, String nome_contato, String cnpj,
			String inscricao) {
		super();
		this.razao_social = razao_social;
		this.telefone = telefone;
		this.celular = celular;
		this.nome_contato = nome_contato;
		this.cnpj = cnpj;
		this.inscricao = inscricao;
	}

	public Fornecedor(String razao_social, String nome_fantasia, String telefone, String celular, String nome_contato,
			String endereco, String numero, String bairro, String cidade, String estado, String cnpj, String inscricao,
			Date dat_ultima_compra) {
		super();
		this.razao_social = razao_social;
		this.nome_fantasia = nome_fantasia;
		this.telefone = telefone;
		this.celular = celular;
		this.nome_contato = nome_contato;
		this.endereco = endereco;
		this.numero = numero;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
		this.cnpj = cnpj;
		this.inscricao = inscricao;
		this.dat_ultima_compra = dat_ultima_compra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRazao_social() {
		return razao_social;
	}

	public void setRazao_social(String razao_social) {
		this.razao_social = razao_social;
	}

	public String getNome_fantasia() {
		return nome_fantasia;
	}

	public void setNome_fantasia(String nome_fantasia) {
		this.nome_fantasia = nome_fantasia;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getNome_contato() {
		return nome_contato;
	}

	public void setNome_contato(String nome_contato) {
		this.nome_contato = nome_contato;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscricao() {
		return inscricao;
	}

	public void setInscricao(String inscricao) {
		this.inscricao = inscricao;
	}

	public Date getDat_ultima_compra() {
		return dat_ultima_compra;
	}

	public void setDat_ultima_compra(Date dat_ultima_compra) {
		this.dat_ultima_compra = dat_ultima_compra;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getNome_fantasia());
		return builder.toString();
	}

}
