package br.edu.unifae.projetointegracao.model;

import java.math.BigDecimal;
import java.util.Date;

public class Cliente {

	private String bairro;

	private String celular;

	private String cidade;

	private String cpf;

	private Date dat_nascimento;

	private Date dat_ultima_venda;

	private String endereco;

	private String estado;

	private Integer id;

	private BigDecimal limite_credito;

	private String nome;

	private String numero;

	private String rg;

	private String telefone;

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDat_nascimento() {
		return dat_nascimento;
	}

	public void setDat_nascimento(Date dat_nascimento) {
		this.dat_nascimento = dat_nascimento;
	}

	public Date getDat_ultima_venda() {
		return dat_ultima_venda;
	}

	public void setDat_ultima_venda(Date dat_ultima_venda) {
		this.dat_ultima_venda = dat_ultima_venda;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getLimite_credito() {
		return limite_credito;
	}

	public void setLimite_credito(BigDecimal limite_credito) {
		this.limite_credito = limite_credito;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Cliente() {

	}

	public Cliente(String nome, Date dat_nascimento, String cpf, String celular, BigDecimal limite_credito) {
		super();
		this.nome = nome;
		this.dat_nascimento = dat_nascimento;
		this.cpf = cpf;
		this.celular = celular;
		this.limite_credito = limite_credito;
	}


	
	
	

	public Cliente(String bairro, String celular, String cidade, String cpf, Date dat_nascimento, Date dat_ultima_venda,
			String endereco, String estado, BigDecimal limite_credito, String nome, String numero, String rg,
			String telefone) {
		super();
		this.bairro = bairro;
		this.celular = celular;
		this.cidade = cidade;
		this.cpf = cpf;
		this.dat_nascimento = dat_nascimento;
		this.dat_ultima_venda = dat_ultima_venda;
		this.endereco = endereco;
		this.estado = estado;
		this.limite_credito = limite_credito;
		this.nome = nome;
		this.numero = numero;
		this.rg = rg;
		this.telefone = telefone;
	}
	
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("");
		builder.append(bairro);
		
		builder.append(celular);
		
		builder.append(cidade);
		
		builder.append(cpf);
		
		builder.append(dat_nascimento);
		
		builder.append(dat_ultima_venda);
		
		builder.append(endereco);
		
		builder.append(estado);
		
		builder.append(id);
		
		builder.append(limite_credito);
		
		builder.append(nome);
		
		builder.append(numero);
		
		builder.append(rg);
		
		builder.append(telefone);
		builder.append("\n");
		return builder.toString();
	}

}
