package br.edu.unifae.projetointegracao.model.generico;

import java.util.List;

import br.edu.unifae.projetointegracao.exception.PersistenciaExcpetion;

public interface GenericoDAO<T> {
	
	int Save(T obj) throws PersistenciaExcpetion;
	
	int deletar(int id) throws PersistenciaExcpetion;
	
	List<T> listarTodos() throws PersistenciaExcpetion;
	
	T findById(int id) throws PersistenciaExcpetion;


	
	
}
