package br.edu.unifae.projetointegracao.model;

import java.util.Date;

public class Produto {

	private Integer id;

	private String nome;

	private String codigoBarra;

	private Double valorCusto;

	private Double valorVenda;

	private Double margemLucro;

	private Integer estoque;

	private Fornecedor fornecedor;

	private Date ultimaVenda;

	private Date ultimaCompra;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Produto [id=");
		builder.append(id);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", codigoBarra=");
		builder.append(codigoBarra);
		builder.append(", valorCusto=");
		builder.append(valorCusto);
		builder.append(", valorVenda=");
		builder.append(valorVenda);
		builder.append(", margemLucro=");
		builder.append(margemLucro);
		builder.append(", estoque=");
		builder.append(estoque);
		builder.append(", idFornecedor=");
		builder.append(fornecedor);
		builder.append(", ultimaVenda=");
		builder.append(ultimaVenda);
		builder.append(", ultimaCompra=");
		builder.append(ultimaCompra);
		builder.append("]");
		return builder.toString();
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigoBarra() {
		return codigoBarra;
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public Double getValorCusto() {
		return valorCusto;
	}

	public void setValorCusto(Double valorCusto) {
		this.valorCusto = valorCusto;
	}

	public Double getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}

	public Double getMargemLucro() {
		return margemLucro;
	}

	public void setMargemLucro(Double margemLucro) {
		this.margemLucro = margemLucro;
	}

	public Integer getEstoque() {
		return estoque;
	}

	public void setEstoque(Integer estoque) {
		this.estoque = estoque;
	}

	public Date getUltimaVenda() {
		return ultimaVenda;
	}

	public void setUltimaVenda(Date ultimaVenda) {
		this.ultimaVenda = ultimaVenda;
	}

	public Date getUltimaCompra() {
		return ultimaCompra;
	}

	public void setUltimaCompra(Date ultimaCompra) {
		this.ultimaCompra = ultimaCompra;
	}

}
