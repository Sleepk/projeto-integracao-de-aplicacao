package br.edu.unifae.projetointegracao.validador;

import br.edu.unifae.projetointegracao.dao.ClienteDAO;
import br.edu.unifae.projetointegracao.exception.NegocioException;
import br.edu.unifae.projetointegracao.exception.ValidacaoException;
import br.edu.unifae.projetointegracao.model.Cliente;

public class ClienteValidador {

	public boolean validaNome(String nome) throws ValidacaoException {
		boolean ehValido = true;
		if (nome == null || nome.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo nome � obrigat�rio!");

		} else if (nome.length() < 3 || nome.length() > 30) {
			ehValido = false;
			throw new ValidacaoException("Campo nome comporta no minimo 3 ou  m�ximo 30 letras!");
		}
		return ehValido;
	}


	public boolean validaCpf(String cpf) throws ValidacaoException {
		boolean ehValido = true;
		if (cpf == null || cpf.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo CPF � obrigat�rio!");
		} else if (cpf.length() != 11) {
			ehValido = false;
			throw new ValidacaoException("Campo CPF deve ter 11 d�gitos!");
		} else {
			char[] digitos = cpf.toCharArray();
			for (char digito : digitos) {
				if (!Character.isDigit(digito)) {
					ehValido = false;
					throw new ValidacaoException("Campo CPF � somente num�rico!");
				}
			}
		}
		return ehValido;
	}

	public Cliente buscaPorId(int id) throws NegocioException {
		Cliente cliente = null;
		try {
			ClienteDAO clienteDAO = new ClienteDAO();
			cliente = clienteDAO.findById(id);
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
		return cliente;
	}

}
