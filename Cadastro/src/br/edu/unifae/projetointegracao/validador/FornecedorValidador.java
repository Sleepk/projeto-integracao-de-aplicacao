package br.edu.unifae.projetointegracao.validador;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import br.edu.unifae.projetointegracao.exception.ValidacaoException;

public class FornecedorValidador {
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");



	public boolean validaNome(String nome) throws ValidacaoException {
		boolean ehValido = true;
		if (nome == null || nome.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo nome � obrigat�rio!");
		} else if (nome.length() > 30) {
			ehValido = false;
			throw new ValidacaoException("Campo nome comporta no m�ximo 30 chars!");
		}
		return ehValido;
	}

	public boolean validaUltimaCompra(String limiteCredito) throws ValidacaoException {
		boolean ehValido = true;
		if (limiteCredito == null || limiteCredito.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo Limite de Credito � obrigat�rio!");
		} else {
			char[] digitos = limiteCredito.toCharArray();
			for (char digito : digitos) {
				if (!Character.isDigit(digito)) {
					ehValido = false;
					throw new ValidacaoException("Campo Limite de Credito � somente num�rico!");
				}
			}
		}
		return ehValido;
	}

	public boolean validaCpf(String cpf) throws ValidacaoException {
		boolean ehValido = true;
		if (cpf == null || cpf.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo CPF � obrigat�rio!");
		} else if (cpf.length() != 11) {
			ehValido = false;
			throw new ValidacaoException("Campo CPF deve ter 11 d�gitos!");
		} else {
			char[] digitos = cpf.toCharArray();
			for (char digito : digitos) {
				if (!Character.isDigit(digito)) {
					ehValido = false;
					throw new ValidacaoException("Campo CPF � somente num�rico!");
				}
			}
		}
		return ehValido;
	}

	public boolean validaDtNasc(String dtNasc) throws ValidacaoException {
		boolean ehValido = true;
		if (dtNasc == null || dtNasc.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo Dt. Nasc. � obrigat�rio!");
		} else {
			ehValido = false;
			try {
				dateFormat.parse(dtNasc);
			} catch (ParseException e) {
				throw new ValidacaoException("Formato inv�lido de data!");
			}
		}
		return ehValido;
	}




}
