package br.edu.unifae.projetointegracao.views;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.MaskFormatter;

import com.toedter.calendar.JDateChooser;

import br.com.caelum.stella.format.CPFFormatter;
import br.com.caelum.stella.validation.CPFValidator;
import br.edu.unifae.projetointegracao.dao.ClienteDAO;
import br.edu.unifae.projetointegracao.exception.NegocioException;
import br.edu.unifae.projetointegracao.exception.PersistenciaExcpetion;
import br.edu.unifae.projetointegracao.exception.ValidacaoException;
import br.edu.unifae.projetointegracao.model.Cliente;
import br.edu.unifae.projetointegracao.service.ClienteService;
import br.edu.unifae.projetointegracao.util.MensagensUtil;
import br.edu.unifae.projetointegracao.validador.ClienteValidador;
import br.edu.unifae.view.jtable.ClienteJTable;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class FrmCadastroCliente extends JInternalFrame {

	private static final long serialVersionUID = 6109239845234443840L;
	private JTextField jtxtNome;
	private JFormattedTextField jtxtCpf;
	private JTextField jtxtRg;
	private JTextField jtxtTelefone;
	private JTextField jtxtCelular;
	private JTextField jtxtLimiteCredito;
	private JTextField jtxtEndereco;
	private JTextField jtxtNumero;
	private JTextField jtxtBairro;
	private JTextField jtxtCidade;
	private JTextField jtxtEstado;
	private ClienteJTable tableCliente;
	private JDateChooser dateTimerChooser;
	private URL iconURL;
	private ImageIcon icon;
	private JDateChooser dateChooser;
	private JLabel lblId;
	public String escreveArquivoCliente = "cadastrocliente.txt";
	private JTextField jtxtFiltroPersonalizado;
	private JComboBox jcmbModoPesquisa;

	public FrmCadastroCliente() throws ParseException {
		setClosable(true);
		setTitle("Cadastro de Clientes");
		setBounds(100, 100, 1334, 533);
		getContentPane().setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 1285, 458);
		getContentPane().add(tabbedPane);

		JPanel pnlCadastro = new JPanel();
		tabbedPane.addTab("Cadastro", null, pnlCadastro, null);
		pnlCadastro.setLayout(null);

		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(57, 14, 27, 14);
		pnlCadastro.add(lblNome);

		JLabel lblData_Nascimento = new JLabel("Data de Nascimento");
		lblData_Nascimento.setBounds(57, 39, 103, 14);
		pnlCadastro.add(lblData_Nascimento);

		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(57, 69, 46, 14);
		pnlCadastro.add(lblCpf);

		JLabel lblRg = new JLabel("RG");
		lblRg.setBounds(57, 94, 46, 14);
		pnlCadastro.add(lblRg);

		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(57, 119, 46, 14);
		pnlCadastro.add(lblTelefone);

		JLabel lblCelular = new JLabel("Celular");
		lblCelular.setBounds(57, 144, 46, 14);
		pnlCadastro.add(lblCelular);

		JLabel lblLimiteDeCredito = new JLabel("Limite de Credito");
		lblLimiteDeCredito.setBounds(57, 169, 80, 14);
		pnlCadastro.add(lblLimiteDeCredito);

		JLabel lblEndereco = new JLabel("Endere�o");
		lblEndereco.setBounds(57, 194, 46, 14);
		pnlCadastro.add(lblEndereco);

		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setBounds(57, 219, 46, 14);
		pnlCadastro.add(lblNumero);

		JLabel lblBairro = new JLabel("Bairro");
		lblBairro.setBounds(57, 244, 46, 14);
		pnlCadastro.add(lblBairro);

		JLabel lblCidade = new JLabel("Cidade");
		lblCidade.setBounds(57, 269, 46, 14);
		pnlCadastro.add(lblCidade);

		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setBounds(57, 294, 46, 14);
		pnlCadastro.add(lblEstado);

		JLabel lblDataDaUltima = new JLabel("Data da Ultima Venda");
		lblDataDaUltima.setBounds(57, 319, 103, 14);
		pnlCadastro.add(lblDataDaUltima);

		jtxtNome = new JTextField();
		jtxtNome.setBounds(188, 11, 193, 20);

		pnlCadastro.add(jtxtNome);
		jtxtNome.setColumns(10);

		dateChooser = new JDateChooser(null, new Date(), null, null);
		dateChooser.setLocale(new Locale("pt", "BR"));
		dateChooser.setDateFormatString("dd. MMMM yyyy");

		dateChooser.setPreferredSize(new Dimension(130, 20));
		dateChooser.setFont(new Font("Verdana", Font.PLAIN, 10));
		dateChooser.setDateFormatString("dd/MM/yyyy");

		iconURL = dateChooser.getClass().getResource("/com/toedter/calendar/images/JMonthChooserColor32.gif");
		icon = new ImageIcon(iconURL);
		dateChooser.setIcon(icon);
		dateChooser.setBounds(188, 316, 193, 20);

		dateChooser.setBounds(188, 36, 193, 20);
		pnlCadastro.add(dateChooser);

		MaskFormatter mask = new MaskFormatter("###.###.###-##");
		jtxtCpf = new JFormattedTextField(mask);
		jtxtCpf.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				char tecla = e.getKeyChar();
				if (Character.isAlphabetic(tecla)) {
					e.consume();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
		jtxtCpf.setBounds(188, 66, 193, 20);
		pnlCadastro.add(jtxtCpf);
		jtxtCpf.setColumns(10);

		jtxtRg = new JTextField();
		jtxtRg.setBounds(188, 91, 193, 20);
		pnlCadastro.add(jtxtRg);
		jtxtRg.setColumns(10);

		jtxtTelefone = new JTextField();
		jtxtTelefone.setBounds(188, 116, 193, 20);
		pnlCadastro.add(jtxtTelefone);
		jtxtTelefone.setColumns(10);

		mask = new MaskFormatter("(##)#####-####");
		jtxtCelular = new JFormattedTextField(mask);
		jtxtCelular.setBounds(188, 141, 193, 20);
		pnlCadastro.add(jtxtCelular);
		jtxtCelular.setColumns(10);

		jtxtLimiteCredito = new JTextField();
		jtxtLimiteCredito.setBounds(188, 166, 193, 20);
		pnlCadastro.add(jtxtLimiteCredito);
		jtxtLimiteCredito.setColumns(10);

		jtxtEndereco = new JTextField();
		jtxtEndereco.setBounds(188, 191, 193, 20);
		pnlCadastro.add(jtxtEndereco);
		jtxtEndereco.setColumns(10);

		jtxtNumero = new JTextField();
		jtxtNumero.setBounds(188, 216, 193, 20);
		pnlCadastro.add(jtxtNumero);
		jtxtNumero.setColumns(10);

		jtxtBairro = new JTextField();
		jtxtBairro.setBounds(188, 241, 193, 20);
		pnlCadastro.add(jtxtBairro);
		jtxtBairro.setColumns(10);

		jtxtCidade = new JTextField();
		jtxtCidade.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char tecla = e.getKeyChar();
				if (!Character.isAlphabetic(tecla)) {
					e.consume();
				}
			}
		});
		jtxtCidade.setBounds(188, 266, 193, 20);
		pnlCadastro.add(jtxtCidade);
		jtxtCidade.setColumns(10);

		jtxtEstado = new JTextField();
		jtxtEstado.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char tecla = e.getKeyChar();
				if (!Character.isAlphabetic(tecla)) {
					e.consume();
				}
			}
		});
		jtxtEstado.setBounds(188, 291, 193, 20);
		pnlCadastro.add(jtxtEstado);
		jtxtEstado.setColumns(10);

		dateTimerChooser = new JDateChooser(null, new Date(), null, null);
		dateTimerChooser.setLocale(new Locale("pt", "BR"));
		dateTimerChooser.setDateFormatString("dd. MMMM yyyy");

		dateTimerChooser.setPreferredSize(new Dimension(130, 20));
		dateTimerChooser.setFont(new Font("Verdana", Font.PLAIN, 10));
		dateTimerChooser.setDateFormatString("dd/MM/yyyy HH:mm:ss");

		iconURL = dateTimerChooser.getClass().getResource("/com/toedter/calendar/images/JMonthChooserColor32.gif");
		icon = new ImageIcon(iconURL);
		dateTimerChooser.setIcon(icon);
		dateTimerChooser.setBounds(188, 316, 193, 20);
		pnlCadastro.add(dateTimerChooser);

		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				Cliente cliente = new Cliente();

				try {
					carregarComponentes(cliente);
					ClienteService clienteService = new ClienteService();
					clienteService.cadastrar(cliente);
					limparBotoes();

					MensagensUtil.addMsg(FrmCadastroCliente.this, "Cadastro efetuado com sucesso!");
					carregarTabela(tableCliente);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(FrmCadastroCliente.this,
							"Erro ao Realizar a Opera��o:\n" + e1.getLocalizedMessage(), "Verificar o Erro Ocorrido",
							JOptionPane.ERROR_MESSAGE);
				}

			}

		});
		btnSalvar.setBounds(628, 396, 89, 23);
		pnlCadastro.add(btnSalvar);

		lblId = new JLabel("id");
		lblId.setBounds(450, 14, 46, 14);
		lblId.setVisible(false);
		pnlCadastro.add(lblId);

		JPanel pnlConsulta = new JPanel();
		tabbedPane.addTab("Consultar", null, pnlConsulta, null);
		pnlConsulta.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 64, 1260, 297);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		pnlConsulta.add(scrollPane);

		tableCliente = new ClienteJTable();
		tableCliente.setFillsViewportHeight(true);

		tableCliente.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tableCliente);

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(tableCliente, popupMenu);

		JMenuItem mntmEditar = new JMenuItem("Editar");
		mntmEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {

				populaItens(tableCliente.getClienteSelecionado());

			}
		});
		popupMenu.add(mntmEditar);

		JMenuItem mntmDeletar = new JMenuItem("Deletar");
		mntmDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Cliente clienteSelecionado = tableCliente.getClienteSelecionado();
				try {
					int showConfirmDialog = JOptionPane.showConfirmDialog(FrmCadastroCliente.this,
							"Deseja Realmente deletar o cliente? ", "Deletar Registro", JOptionPane.WARNING_MESSAGE);
					if (showConfirmDialog == 0) {
						new ClienteService().removePessoa(clienteSelecionado.getId());
						MensagensUtil.addMsg(null, "Pessoa deletada com sucesso!");
					} else {
						MensagensUtil.addMsg(FrmCadastroCliente.this, "Opera��o Cancelada");
					}
					carregarTabela(tableCliente);
				} catch (NegocioException e1) {
					e1.printStackTrace();
				}
			}
		});
		popupMenu.add(mntmDeletar);

		JButton btnNewButton_1 = new JButton("Pesquisar");
		btnNewButton_1.setBounds(722, 17, 89, 23);
		btnNewButton_1.addActionListener(new ActionListener() {

			ClienteService service = new ClienteService();

			public void actionPerformed(ActionEvent e) {
				if (jcmbModoPesquisa.getSelectedItem().equals("nome")) {
					try {
						List<Cliente> buscaPorNome = service
								.buscaPorNome(jtxtFiltroPersonalizado.getText().toUpperCase());
						carregarTabelaPersonalizada(tableCliente, buscaPorNome);
					} catch (NegocioException | PersistenciaExcpetion e1) {
						MensagensUtil.addMsg(FrmCadastroCliente.this, "Este nome nao existe em nossa base de dados");
					}
				} 
			}
		});
		pnlConsulta.add(btnNewButton_1);

		carregarTabela(tableCliente);

		JLabel label = new JLabel("Pesquisar por:");
		label.setBounds(15, 21, 97, 14);
		pnlConsulta.add(label);

		jcmbModoPesquisa = new JComboBox();
		jcmbModoPesquisa.setModel(new DefaultComboBoxModel(
				new String[] { "nome", "rg", "telefone", "celular", "limite credito", "endereco", "numero", "bairro",
						"cidade", "estado", "data ultima venda", "cpf", "data nascimento" }));
		jcmbModoPesquisa.setBounds(90, 16, 135, 24);
		pnlConsulta.add(jcmbModoPesquisa);

		jtxtFiltroPersonalizado = new JTextField();
		jtxtFiltroPersonalizado.setBounds(239, 11, 473, 29);
		pnlConsulta.add(jtxtFiltroPersonalizado);
		jtxtFiltroPersonalizado.setColumns(10);

	}

	/**
	 * Funcao que vai fazer o carregamento dos itens da tabela.
	 * 
	 * @param tabela
	 */
	private void carregarTabela(ClienteJTable tabela) {
		try {
			tabela.load(new ClienteDAO().listarTodos());
		} catch (PersistenciaExcpetion e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
					"Houve um erro ao carregar as pessoas no banco de dados!" + "\n" + e.getMessage());
		}

	}

	private void carregarTabelaPersonalizada(ClienteJTable tabela, List<Cliente> item) throws PersistenciaExcpetion {
		tabela.load(item);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	private void carregarComponentes(Cliente cliente) throws ValidacaoException {
		ClienteValidador clienteBO = new ClienteValidador();
		Date dataUltimaCompra = dateTimerChooser.getCalendar().getTime();
		Date dataNascimento = dateChooser.getCalendar().getTime();

		clienteBO.validaNome(jtxtNome.getText());

		CPFValidator cpfValidador = new CPFValidator();
		cpfValidador.assertValid(jtxtCpf.getText());
		CPFFormatter formatadorCPF = new CPFFormatter();

		// 86288366757
		if (!lblId.getText().contains("id")) {
			cliente.setId(Integer.parseInt(lblId.getText()));
		}
		cliente.setBairro(jtxtBairro.getText());
		cliente.setCelular(jtxtCelular.getText().replace("(", "").replace(")", "").replace("-", ""));
		cliente.setCidade(jtxtCidade.getText());
		cliente.setCpf(formatadorCPF.unformat(jtxtCpf.getText()));
		cliente.setDat_nascimento(dataNascimento);
		cliente.setDat_ultima_venda(dataUltimaCompra);
		cliente.setEndereco(jtxtEndereco.getText());
		cliente.setEstado(jtxtEstado.getText());
		cliente.setLimite_credito(new BigDecimal(jtxtLimiteCredito.getText()));
		cliente.setNome(jtxtNome.getText());
		cliente.setNumero(jtxtNumero.getText());
		cliente.setRg(jtxtRg.getText());
		cliente.setTelefone(jtxtTelefone.getText());
	}

	private void populaItens(Cliente cliente) {

		lblId.setText(cliente.getId().toString());
		DecimalFormat resultado = new DecimalFormat("0");
		jtxtBairro.setText(cliente.getBairro());
		jtxtCelular.setText(cliente.getCelular());
		jtxtCidade.setText(cliente.getCidade());
		jtxtCpf.setText(cliente.getCpf());
		jtxtEndereco.setText(cliente.getEndereco());
		jtxtEstado.setText(cliente.getEstado());
		jtxtLimiteCredito.setText(resultado.format(cliente.getLimite_credito()));
		jtxtNome.setText(cliente.getNome());
		jtxtNumero.setText(cliente.getNumero());
		jtxtRg.setText(cliente.getRg());
		jtxtTelefone.setText(cliente.getTelefone());

		dateChooser.setDate(cliente.getDat_nascimento());
		dateTimerChooser.setDate(cliente.getDat_ultima_venda());
	}

	private void limparBotoes() {
		lblId.setText("id");
		jtxtBairro.setText("");
		jtxtCelular.setText("");
		jtxtCidade.setText("");
		jtxtCpf.setText("");
		jtxtEndereco.setText("");
		jtxtEstado.setText("");
		jtxtLimiteCredito.setText("");
		jtxtNome.setText("");
		jtxtNumero.setText("");
		jtxtRg.setText("");
		jtxtTelefone.setText("");

		dateChooser.setEnabled(true);
		dateTimerChooser.setEnabled(true);
	}
}
