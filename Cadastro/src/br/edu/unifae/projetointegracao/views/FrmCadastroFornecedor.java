package br.edu.unifae.projetointegracao.views;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import com.toedter.calendar.JDateChooser;

import br.com.caelum.stella.format.CNPJFormatter;
import br.com.caelum.stella.validation.CNPJValidator;
import br.edu.unifae.projetointegracao.dao.FornecedorDAO;
import br.edu.unifae.projetointegracao.exception.NegocioException;
import br.edu.unifae.projetointegracao.exception.PersistenciaExcpetion;
import br.edu.unifae.projetointegracao.exception.ValidacaoException;
import br.edu.unifae.projetointegracao.model.Fornecedor;
import br.edu.unifae.projetointegracao.service.FornecedorService;
import br.edu.unifae.projetointegracao.util.MensagensUtil;
import br.edu.unifae.projetointegracao.validador.FornecedorValidador;
import br.edu.unifae.view.jtable.FornecedorJTable;

public class FrmCadastroFornecedor extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1369930807786157697L;
	private JTextField jtxtRazao_social;
	private JTextField jtxtNome_Fantasia;
	private JTextField jtxtNome_Contato;
	private JTextField jtxtCnpj;
	private JTextField jtxtTelefone;
	private JTextField jtxtCelular;
	private JTextField jtxtInscricao;
	private JTextField jtxtEndereco;
	private JTextField jtxtNumero;
	private JTextField jtxtBairro;
	private JTextField jtxtCidade;
	private JTextField jtxtEstado;
	private FornecedorJTable tableFornecedor;
	private JDateChooser dateTimerChooser;
	private URL iconURL;
	private ImageIcon icon;
	private JLabel lblId;
	private MaskFormatter mask;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 * 
	 * @throws ParseException
	 */
	public FrmCadastroFornecedor() throws ParseException {
		setTitle("Cadastro de Fornecedor");
		setClosable(true);
		setBounds(100, 100, 1103, 510);
		getContentPane().setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 1086, 473);
		getContentPane().add(tabbedPane);

		JPanel pnlCadastro = new JPanel();
		tabbedPane.addTab("Cadastro", null, pnlCadastro, null);
		pnlCadastro.setLayout(null);

		JLabel lblRazao_Social = new JLabel("Raz\u00E3o Social");
		lblRazao_Social.setBounds(57, 14, 145, 14);
		pnlCadastro.add(lblRazao_Social);

		JLabel lblNome_Fantasia = new JLabel("Nome Fantasia");
		lblNome_Fantasia.setBounds(57, 39, 145, 14);
		pnlCadastro.add(lblNome_Fantasia);

		JLabel lblNome_contato = new JLabel("Nome Contato");
		lblNome_contato.setBounds(57, 69, 145, 14);
		pnlCadastro.add(lblNome_contato);

		JLabel lblCnpj = new JLabel("CNPJ");
		lblCnpj.setBounds(57, 94, 145, 14);
		pnlCadastro.add(lblCnpj);

		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(57, 119, 145, 14);
		pnlCadastro.add(lblTelefone);

		JLabel lblCelular = new JLabel("Celular");
		lblCelular.setBounds(57, 144, 145, 14);
		pnlCadastro.add(lblCelular);

		JLabel lblInscricao = new JLabel("Inscri\u00E7\u00E3o");
		lblInscricao.setBounds(57, 169, 145, 14);
		pnlCadastro.add(lblInscricao);

		JLabel lblEndereco = new JLabel("Endere\u00E7o");
		lblEndereco.setBounds(57, 194, 145, 14);
		pnlCadastro.add(lblEndereco);

		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setBounds(57, 219, 145, 14);
		pnlCadastro.add(lblNumero);

		JLabel lblBairro = new JLabel("Bairro");
		lblBairro.setBounds(57, 244, 145, 14);
		pnlCadastro.add(lblBairro);

		JLabel lblCidade = new JLabel("Cidade");
		lblCidade.setBounds(57, 269, 145, 14);
		pnlCadastro.add(lblCidade);

		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setBounds(57, 294, 145, 14);
		pnlCadastro.add(lblEstado);

		JLabel lblDataDaUltimaCompra = new JLabel("Data da Ultima Compra");
		lblDataDaUltimaCompra.setBounds(57, 319, 145, 14);
		pnlCadastro.add(lblDataDaUltimaCompra);

		jtxtRazao_social = new JTextField();
		jtxtRazao_social.setBounds(272, 11, 193, 20);
		pnlCadastro.add(jtxtRazao_social);
		jtxtRazao_social.setColumns(10);

		jtxtNome_Fantasia = new JTextField();
		jtxtNome_Fantasia.setBounds(272, 36, 193, 20);
		pnlCadastro.add(jtxtNome_Fantasia);
		jtxtNome_Fantasia.setColumns(10);

		jtxtNome_Contato = new JTextField();
		jtxtNome_Contato.setBounds(272, 66, 193, 20);
		pnlCadastro.add(jtxtNome_Contato);
		jtxtNome_Contato.setColumns(10);

		mask = new MaskFormatter("##.###.###/####-##");
		jtxtCnpj = new JFormattedTextField(mask);
		jtxtCnpj.setBounds(272, 91, 193, 20);
		pnlCadastro.add(jtxtCnpj);
		jtxtCnpj.setColumns(10);

		jtxtTelefone = new JTextField();
		jtxtTelefone.setBounds(272, 116, 193, 20);
		pnlCadastro.add(jtxtTelefone);
		jtxtTelefone.setColumns(10);

		jtxtCelular = new JTextField();
		jtxtCelular.setBounds(272, 141, 193, 20);
		pnlCadastro.add(jtxtCelular);
		jtxtCelular.setColumns(10);

		jtxtInscricao = new JTextField();
		jtxtInscricao.setBounds(272, 166, 193, 20);
		pnlCadastro.add(jtxtInscricao);
		jtxtInscricao.setColumns(10);

		jtxtEndereco = new JTextField();
		jtxtEndereco.setBounds(272, 191, 193, 20);
		pnlCadastro.add(jtxtEndereco);
		jtxtEndereco.setColumns(10);

		jtxtNumero = new JTextField();
		jtxtNumero.setBounds(272, 216, 193, 20);
		pnlCadastro.add(jtxtNumero);
		jtxtNumero.setColumns(10);

		jtxtBairro = new JTextField();
		jtxtBairro.setBounds(272, 241, 193, 20);
		pnlCadastro.add(jtxtBairro);
		jtxtBairro.setColumns(10);

		jtxtCidade = new JTextField();
		jtxtCidade.setBounds(272, 266, 193, 20);
		pnlCadastro.add(jtxtCidade);
		jtxtCidade.setColumns(10);

		jtxtEstado = new JTextField();
		jtxtEstado.setBounds(272, 291, 193, 20);
		pnlCadastro.add(jtxtEstado);
		jtxtEstado.setColumns(10);

		dateTimerChooser = new JDateChooser(null, new Date(), null, null);
		dateTimerChooser.setLocale(new Locale("pt", "BR"));
		dateTimerChooser.setPreferredSize(new Dimension(130, 20));
		dateTimerChooser.setFont(new Font("Verdana", Font.PLAIN, 10));
		dateTimerChooser.setDateFormatString("dd/MM/yyyy HH:mm:ss");

		iconURL = dateTimerChooser.getClass().getResource("/com/toedter/calendar/images/JMonthChooserColor32.gif");
		icon = new ImageIcon(iconURL);
		dateTimerChooser.setIcon(icon);
		dateTimerChooser.setBounds(272, 316, 193, 20);
		pnlCadastro.add(dateTimerChooser);

		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Fornecedor fornecedor = new Fornecedor();

				try {

					carregarComponentes(fornecedor);
					FornecedorService fornecedorService = new FornecedorService();

					fornecedorService.cadastrar(fornecedor);
					carregarTabela(tableFornecedor);
					limparBotoes();
					MensagensUtil.addMsg(FrmCadastroFornecedor.this, "Cadastro efetuado com sucesso!");
				} catch (Exception e1) {
					e1.printStackTrace();
					MensagensUtil.addMsg(FrmCadastroFornecedor.this, e1.getMessage());
				}

			}
		});

		btnSalvar.setBounds(483, 397, 89, 23);
		pnlCadastro.add(btnSalvar);

		lblId = new JLabel("id");
		lblId.setEnabled(false);
		lblId.setVisible(false);
		lblId.setBounds(500, 14, 46, 14);
		pnlCadastro.add(lblId);

		JPanel pnlConsulta = new JPanel();
		tabbedPane.addTab("Consultar", null, pnlConsulta, null);
		pnlConsulta.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 31, 1039, 372);
		pnlConsulta.add(scrollPane);

		tableFornecedor = new FornecedorJTable();
		scrollPane.setViewportView(tableFornecedor);
		carregarTabela(tableFornecedor);

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(tableFornecedor, popupMenu);

		JMenuItem mntmEditar = new JMenuItem("Editar");
		mntmEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				populaItens(tableFornecedor.getFornecedorSelecionado());
			}
		});
		popupMenu.add(mntmEditar);

		JMenuItem mntmDeletar = new JMenuItem("Deletar");
		mntmDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Fornecedor fornecedorSelecionado = tableFornecedor.getFornecedorSelecionado();
				try {
					int showConfirmDialog = JOptionPane.showConfirmDialog(FrmCadastroFornecedor.this,
							"Deseja Realmente deletar o fornecedor? ", "Deletar Registro", JOptionPane.WARNING_MESSAGE);
					if (showConfirmDialog == 0) {
						new FornecedorService().removeFornecedor(fornecedorSelecionado.getId());
						MensagensUtil.addMsg(FrmCadastroFornecedor.this, "Pessoa deletada com sucesso!");
					} else {
						MensagensUtil.addMsg(FrmCadastroFornecedor.this, "Opera��o Cancelada");
					}

					carregarTabela(tableFornecedor);
				} catch (NegocioException e1) {
					e1.printStackTrace();
				}
			}
		});
		popupMenu.add(mntmDeletar);

	}

	private void carregarTabela(FornecedorJTable tabela) {
		try {
			tabela.load(new FornecedorDAO().listarTodos());
		} catch (PersistenciaExcpetion e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
					"Houve um erro ao carregar as pessoas no banco de dados!" + "\n" + e.getMessage());
		}

	}

	private void populaItens(Fornecedor fornecedor) {

		lblId.setText(fornecedor.getId().toString());
		jtxtBairro.setText(fornecedor.getBairro());
		jtxtCelular.setText(fornecedor.getCelular());
		jtxtCidade.setText(fornecedor.getCidade());
		jtxtCnpj.setText(fornecedor.getCnpj());
		jtxtCnpj.setText(fornecedor.getCnpj());
		jtxtEndereco.setText(fornecedor.getEndereco());
		jtxtEstado.setText(fornecedor.getEstado());
		jtxtInscricao.setText(fornecedor.getInscricao());
		jtxtNome_Contato.setText(fornecedor.getNome_contato());
		jtxtNome_Fantasia.setText(fornecedor.getNome_fantasia());
		jtxtNumero.setText(fornecedor.getNumero());
		jtxtRazao_social.setText(fornecedor.getRazao_social());
		jtxtTelefone.setText(fornecedor.getTelefone());

		dateTimerChooser.setDate(fornecedor.getDat_ultima_compra());

	}

	private void carregarComponentes(Fornecedor fornecedor) throws ValidacaoException {
		FornecedorValidador fornecedorBO = new FornecedorValidador();
		Date dataUltimaCompra = dateTimerChooser.getCalendar().getTime();

		fornecedorBO.validaNome(jtxtNome_Contato.getText());
		// clienteBO.validaUltimaCompra(jtxtLimiteCredito.getText());

		CNPJValidator cnpjValidador = new CNPJValidator();
		cnpjValidador.assertValid(jtxtCnpj.getText());
		CNPJFormatter formatadorCNPJ = new CNPJFormatter();

		// 86288366757

		if (!lblId.getText().contains("id")) {
			fornecedor.setId(Integer.parseInt(lblId.getText()));
		}
		fornecedor.setBairro(jtxtBairro.getText());
		fornecedor.setCelular(jtxtCelular.getText());
		fornecedor.setCidade(jtxtCidade.getText());
		fornecedor.setCnpj(formatadorCNPJ.unformat(jtxtCnpj.getText()));
		fornecedor.setDat_ultima_compra(dataUltimaCompra);
		fornecedor.setEndereco(jtxtEndereco.getText());
		fornecedor.setEstado(jtxtEstado.getText());
		fornecedor.setNome_contato(jtxtNome_Contato.getText());
		fornecedor.setNome_fantasia(jtxtNome_Fantasia.getText());
		fornecedor.setNumero(jtxtNumero.getText());
		fornecedor.setRazao_social(jtxtRazao_social.getText());
		fornecedor.setTelefone(jtxtTelefone.getText());
		fornecedor.setInscricao(jtxtInscricao.getText());

	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	
	public static void escritor(String path, Fornecedor fornecedor) throws IOException {
		BufferedWriter buffWrite = new BufferedWriter(new FileWriter(path,true));
		buffWrite.append(fornecedor.toString() + "\n");
		buffWrite.close();
	}

	public void limparBotoes() {
		lblId.setText("id");
		jtxtBairro.setText("");
		jtxtCelular.setText("");
		jtxtCidade.setText("");
		jtxtCnpj.setText("");
		jtxtCnpj.setText("");
		jtxtEndereco.setText("");
		jtxtEstado.setText("");
		jtxtInscricao.setText("");
		jtxtNome_Contato.setText("");
		jtxtNome_Fantasia.setText("");
		jtxtNumero.setText("");
		jtxtRazao_social.setText("");
		jtxtTelefone.setText("");

	}
}
