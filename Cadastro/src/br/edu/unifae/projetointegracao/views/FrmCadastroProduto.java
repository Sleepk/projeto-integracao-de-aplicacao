package br.edu.unifae.projetointegracao.views;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;

import com.toedter.calendar.JDateChooser;

import br.edu.unifae.projetointegracao.dao.ProdutoDAO;
import br.edu.unifae.projetointegracao.exception.NegocioException;
import br.edu.unifae.projetointegracao.exception.PersistenciaExcpetion;
import br.edu.unifae.projetointegracao.exception.ValidacaoException;
import br.edu.unifae.projetointegracao.model.Fornecedor;
import br.edu.unifae.projetointegracao.model.Produto;
import br.edu.unifae.projetointegracao.service.FornecedorService;
import br.edu.unifae.projetointegracao.service.ProdutoService;
import br.edu.unifae.projetointegracao.util.MensagensUtil;
import br.edu.unifae.view.jtable.ProdutoJTable;

public class FrmCadastroProduto extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField jtxtNome;
	private JTextField jtxtCodigoBarras;
	private JTextField jtxtValorCusto;
	private JTextField jtxtValorVenda;
	private JTextField jtxtMargemLucro;
	private JTextField jtxtEstoque;
	private JPanel pnlConsulta;
	private JPanel pnlCadastro;
	private JDateChooser dateChooserUltimaCompra;
	private JDateChooser dateChooserUltimaVenda;
	private URL iconURL;
	private ImageIcon icon;
	@SuppressWarnings("rawtypes")
	private JComboBox cbxFornecedores;
	private FornecedorService fornecedorService;
	protected Produto produto;
	private ProdutoJTable tableProduto;
	private JLabel lblId;

	/**
	 * Create the frame.
	 * 
	 * @throws NegocioException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public FrmCadastroProduto() throws NegocioException {
		setTitle("Cadastro de Produtos");
		setClosable(true);
		setBounds(100, 100, 902, 430);
		getContentPane().setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 1322, 400);
		getContentPane().add(tabbedPane);

		pnlCadastro = new JPanel();
		tabbedPane.addTab("Cadastro", null, pnlCadastro, null);
		pnlCadastro.setLayout(null);

		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(57, 14, 80, 14);
		pnlCadastro.add(lblNome);

		JLabel lblCodigo_Barras = new JLabel("Codigo de Barras");
		lblCodigo_Barras.setBounds(57, 39, 103, 14);
		pnlCadastro.add(lblCodigo_Barras);

		JLabel lblValor_Custo = new JLabel("Valor de Custo");
		lblValor_Custo.setBounds(57, 69, 80, 14);
		pnlCadastro.add(lblValor_Custo);

		JLabel lblValor_Vendas = new JLabel("Valor de Venda");
		lblValor_Vendas.setBounds(57, 94, 80, 14);
		pnlCadastro.add(lblValor_Vendas);

		JLabel lblMargem_lucro = new JLabel("Margem de Lucro");
		lblMargem_lucro.setBounds(57, 119, 103, 14);
		pnlCadastro.add(lblMargem_lucro);

		JLabel lblEstoque = new JLabel("Estoque");
		lblEstoque.setBounds(57, 144, 46, 14);
		pnlCadastro.add(lblEstoque);

		JLabel lblFornecedores_ID = new JLabel("Fornecedores");
		lblFornecedores_ID.setBounds(57, 169, 80, 14);
		pnlCadastro.add(lblFornecedores_ID);

		JLabel lblUltima_venda = new JLabel("Data da Ultima Venda");
		lblUltima_venda.setBounds(57, 200, 103, 14);
		pnlCadastro.add(lblUltima_venda);

		JLabel lblUltima_Compra = new JLabel("Data da Ultima Compra");
		lblUltima_Compra.setBounds(57, 225, 121, 14);
		pnlCadastro.add(lblUltima_Compra);

		jtxtNome = new JTextField();
		jtxtNome.setBounds(188, 11, 193, 20);
		pnlCadastro.add(jtxtNome);
		jtxtNome.setColumns(10);

		jtxtCodigoBarras = new JTextField();
		jtxtCodigoBarras.setBounds(188, 36, 193, 20);
		pnlCadastro.add(jtxtCodigoBarras);
		jtxtCodigoBarras.setColumns(10);

		jtxtValorCusto = new JTextField();
		jtxtValorCusto.setText("0");
		jtxtValorCusto.setBounds(188, 66, 193, 20);
		pnlCadastro.add(jtxtValorCusto);
		jtxtValorCusto.setColumns(10);

		jtxtValorVenda = new JTextField();
		jtxtValorVenda.setText("0");
		jtxtValorVenda.setBounds(188, 91, 193, 20);
		pnlCadastro.add(jtxtValorVenda);
		jtxtValorVenda.setColumns(10);

		jtxtMargemLucro = new JTextField();
		jtxtMargemLucro.setText("0");
		jtxtMargemLucro.setBounds(188, 116, 193, 20);
		pnlCadastro.add(jtxtMargemLucro);
		jtxtMargemLucro.setColumns(10);

		jtxtEstoque = new JTextField();
		jtxtEstoque.setText("0");
		jtxtEstoque.setBounds(188, 141, 193, 20);
		pnlCadastro.add(jtxtEstoque);
		jtxtEstoque.setColumns(10);

		dateChooserUltimaVenda = new JDateChooser(null, new Date(), null, null);
		dateChooserUltimaVenda.setLocale(new Locale("pt", "BR"));
		dateChooserUltimaVenda.setDateFormatString("dd. MMMM yyyy");

		dateChooserUltimaVenda.setPreferredSize(new Dimension(130, 20));
		dateChooserUltimaVenda.setFont(new Font("Verdana", Font.PLAIN, 10));
		dateChooserUltimaVenda.setDateFormatString("dd/MM/yyyy HH:mm:ss");

		iconURL = dateChooserUltimaVenda.getClass()
				.getResource("/com/toedter/calendar/images/JMonthChooserColor32.gif");
		icon = new ImageIcon(iconURL);
		dateChooserUltimaVenda.setIcon(icon);

		dateChooserUltimaVenda.setBounds(188, 197, 193, 20);
		pnlCadastro.add(dateChooserUltimaVenda);

		dateChooserUltimaCompra = new JDateChooser(null, new Date(), null, null);
		dateChooserUltimaCompra.setLocale(new Locale("pt", "BR"));
		dateChooserUltimaCompra.setDateFormatString("dd. MMMM yyyy");

		dateChooserUltimaCompra.setPreferredSize(new Dimension(130, 20));
		dateChooserUltimaCompra.setFont(new Font("Verdana", Font.PLAIN, 10));
		dateChooserUltimaCompra.setDateFormatString("dd/MM/yyyy HH:mm:ss");

		iconURL = dateChooserUltimaCompra.getClass()
				.getResource("/com/toedter/calendar/images/JMonthChooserColor32.gif");
		icon = new ImageIcon(iconURL);
		dateChooserUltimaCompra.setIcon(icon);
		dateChooserUltimaCompra.setBounds(188, 222, 193, 20);
		pnlCadastro.add(dateChooserUltimaCompra);

		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				produto = new Produto();

				try {
					carregaComponentes(produto);
					ProdutoService produtoService = new ProdutoService();
					produtoService.cadastrar(produto);
					MensagensUtil.addMsg(FrmCadastroProduto.this, "Cadastro efetuado com sucesso!");
					carregarTabela(tableProduto);
				} catch (ValidacaoException | NegocioException e) {
					e.printStackTrace();
					MensagensUtil.addMsg(FrmCadastroProduto.this, "Opa ocorreu um erro: " + e.getMessage());
				}

			}
		});
		btnSalvar.setBounds(514, 338, 89, 23);
		pnlCadastro.add(btnSalvar);

		cbxFornecedores = new JComboBox();
		cbxFornecedores.setModel(new DefaultComboBoxModel(new String[] { "Escolha um fornecedor..." }));
		cbxFornecedores.setBounds(188, 166, 187, 20);
		pnlCadastro.add(cbxFornecedores);

		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.setBounds(394, 90, 89, 23);
		pnlCadastro.add(btnCalcular);

		lblId = new JLabel("id");
		lblId.setBounds(391, 14, 46, 14);
		pnlCadastro.add(lblId);

		fornecedorService = new FornecedorService();

		fornecedorService.buscarTodos().forEach(u -> cbxFornecedores.addItem(u));

		pnlConsulta = new JPanel();
		tabbedPane.addTab("Consulta", null, pnlConsulta, null);
		pnlConsulta.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 64, 843, 297);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		pnlConsulta.add(scrollPane);

		tableProduto = new ProdutoJTable();
		tableProduto.setFillsViewportHeight(true);

		tableProduto.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tableProduto);

		carregarTabela(tableProduto);

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(tableProduto, popupMenu);

		JMenuItem mntmEditar = new JMenuItem("Editar");
		mntmEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				populaItens(tableProduto.getProdutoSelecionado());
			}
		});
		popupMenu.add(mntmEditar);

		JMenuItem mntmDeletar = new JMenuItem("Deletar");
		mntmDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Produto clienteSelecionado = tableProduto.getProdutoSelecionado();
				try {
					int showConfirmDialog = JOptionPane.showConfirmDialog(FrmCadastroProduto.this,
							"Deseja Realmente deletar o cliente? ", "Deletar Registro", JOptionPane.WARNING_MESSAGE);
					if (showConfirmDialog == 0) {
						new ProdutoService().removePessoa(clienteSelecionado.getId());
						JOptionPane.showMessageDialog(null, "Produto deletado com sucesso!");
					} else {
						MensagensUtil.addMsg(FrmCadastroProduto.this, "Opera��o Cancelada");
					}
					carregarTabela(tableProduto);
				} catch (NegocioException e1) {
					e1.printStackTrace();
				}
			}
		});
		popupMenu.add(mntmDeletar);

	}

	public void carregaComponentes(Produto produto) throws ValidacaoException {

		// ProdutoValidador produtoBO = new ProdutoValidador();

		Date dataUltimaCompra = dateChooserUltimaCompra.getCalendar().getTime();
		Date UltimaVenda = dateChooserUltimaVenda.getCalendar().getTime();

		if (!lblId.getText().contains("id")) {
			produto.setId(Integer.parseInt(lblId.getText()));
		}

		produto.setCodigoBarra(jtxtCodigoBarras.getText());
		produto.setEstoque(Integer.parseInt(jtxtEstoque.getText()));
		produto.setFornecedor((Fornecedor) cbxFornecedores.getSelectedItem());
		produto.setMargemLucro(Double.parseDouble(jtxtMargemLucro.getText()));
		produto.setNome(jtxtNome.getText());
		produto.setValorCusto(Double.parseDouble(jtxtValorCusto.getText()));
		produto.setValorVenda(Double.parseDouble(jtxtValorVenda.getText()));
		produto.setUltimaCompra(dataUltimaCompra);
		produto.setUltimaVenda(UltimaVenda);

		System.out.println(produto.getFornecedor());

	}

	private void carregarTabela(ProdutoJTable tabela) {
		try {
			tabela.load(new ProdutoDAO().listarTodos());
		} catch (PersistenciaExcpetion e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
					"Houve um erro ao carregar as pessoas no banco de dados!" + "\n" + e.getMessage());
		}

	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	private void populaItens(Produto produto) {

		DecimalFormat resultado = new DecimalFormat("0,0");
		lblId.setText(produto.getId().toString());
		jtxtCodigoBarras.setText(produto.getCodigoBarra());
		jtxtEstoque.setText(produto.getEstoque().toString());
		jtxtMargemLucro.setText(resultado.format(produto.getMargemLucro()));
		jtxtNome.setText(produto.getNome());
		jtxtValorCusto.setText(resultado.format(produto.getValorCusto()));
		jtxtValorVenda.setText(resultado.format(produto.getValorVenda()));
		cbxFornecedores.setSelectedIndex(produto.getFornecedor().getId() - 1);
		dateChooserUltimaCompra.setDate(produto.getUltimaCompra());
		dateChooserUltimaVenda.setDate(produto.getUltimaVenda());

	}
}
