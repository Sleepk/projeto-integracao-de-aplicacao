package br.edu.unifae.projetointegracao.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import br.edu.unifae.projetointegracao.views.FrmCadastroCliente;
import br.edu.unifae.projetointegracao.views.FrmCadastroFornecedor;
import br.edu.unifae.projetointegracao.views.FrmCadastroProduto;

public class FrmMenuPrincipal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JDesktopPane desktopPane;
	private JMenuItem jmnCliente;
	private JMenuBar menuBar;
	private JMenu mnCadastro;
	protected FrmCadastroCliente frmCadastroCliente;
	protected FrmCadastroFornecedor frmCadastroFornecedor;
	protected FrmCadastroProduto frmCadastroProduto;

	public FrmMenuPrincipal() {
		setTitle("Cadastro Projeto Integracao Tiago");

		int inset = 50;

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(inset, inset, screenSize.width - inset * 2, screenSize.height - inset * 2);

		desktopPane = new JDesktopPane();
		desktopPane.paintComponents(getGraphics());
		desktopPane.setBackground(new Color(211, 211, 211));
		jmnCliente = new JMenuItem("Cliente");
		jmnCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					frmCadastroCliente = new FrmCadastroCliente();
				} catch (Exception e) {
					e.printStackTrace();
				}
				frmCadastroCliente.setVisible(true);
				desktopPane.add(frmCadastroCliente);
			}
		});

		menuBar = new JMenuBar();
		mnCadastro = new JMenu("Cadastro");

		setContentPane(desktopPane);
		desktopPane.setLayout(null);

		mnCadastro.add(jmnCliente);

		menuBar.add(mnCadastro);

		JMenuItem mntmFornecedor = new JMenuItem("Fornecedor");
		mntmFornecedor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					frmCadastroFornecedor = new FrmCadastroFornecedor();
				} catch (Exception e) {
					e.printStackTrace();
				}
				frmCadastroFornecedor.setVisible(true);
				desktopPane.add(frmCadastroFornecedor);
			}
		});
		mnCadastro.add(mntmFornecedor);

		JMenuItem mntmProdutos = new JMenuItem("Produtos");
		mntmProdutos.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				try {
					frmCadastroProduto = new FrmCadastroProduto();
				} catch (Exception e) {
					e.printStackTrace();
				}
				frmCadastroProduto.setVisible(true);
				desktopPane.add(frmCadastroProduto);
			}
		});
		mnCadastro.add(mntmProdutos);

		setJMenuBar(menuBar);

		setVisible(true);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@SuppressWarnings("unused")
	public static void main(String args[]) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException {

		// ha 1 arquivo externo para poder alterar a visualziacao do lookandfeel padrao.
		// com o comando abaixo, isto � feito de forma automatica.
		// WebLookAndFeel.install();
		for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			if ("Windows".equals(info.getName())) {
				UIManager.setLookAndFeel(info.getClassName());
				break;
			}
		}
		FrmMenuPrincipal p = new FrmMenuPrincipal();
	}
}
