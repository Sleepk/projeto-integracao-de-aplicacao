package br.edu.unifae.projetointegracao.conexaoUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
/**
 * Classe responsavel por abrir a conexao do arquivo
 * @author julio
 *
 */
public class ConexaoUtil {
	
	private static ResourceBundle config;

	private static ConexaoUtil conexaoUtil;
	
	private ConexaoUtil() {
		config = ResourceBundle.getBundle("config");
	}
	
	public static ConexaoUtil getInstance() {
		if (conexaoUtil == null) {
			conexaoUtil = new ConexaoUtil();
		}
		return conexaoUtil;
	}
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(config.getString("br.edu.unifae.projetointegracao.bd.driver.mysql"));
		return DriverManager.getConnection(config.getString("br.edu.unifae.projetointegracao.bd.url.conexao"), 
				config.getString("br.edu.unifae.projetointegracao.bd.usuario"), config.getString("br.edu.unifae.projetointegracao.bd.senha"));
	}

	public static void main(String[] args) {
		try {
			
			if(getInstance().getConnection() == null) {
				System.out.println("Null");
			}else {
				
				System.out.println("Nao null");
			}
			System.out.println(getInstance().getConnection().toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
